
<?php  
   ob_start();
   session_start();
   include_once 'config/config.php';
   include_once 'mvc/models/model.php';
   if (isset($_GET['page'])) {
      $page = $_GET['page'];
   }else {
      $page = 'search';
   }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function() {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        }); 
    </script>
<title>Day 12</title>
<style>
@import url("https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap");
</style>
</head>

<?php
$page = "search";
 if (file_exists('mvc/views/'.$page.'.php')) {
  ?>
<body>
<?php 
   
            include_once 'mvc/controllers/controller.php';
            $controll = new Controller();
            $controll->Controllers();
            ?>
</body>
<?php
} else {
  echo "<h2 style='' class='err404'>Trang không tồn tại!</h2>";
}
?>
</html>
