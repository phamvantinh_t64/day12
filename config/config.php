<?php  
	class Connect
	{
		private $dns = "mysql:host=localhost:8889;dbname=day12";
		private $user = "root";
		private $pass = "root";
		protected $pdo = null;

		function __construct()
		{
			try{
				$this->pdo = new PDO($this->dns, $this->user, $this->pass);
				$this->pdo->query("SET NAMES utf8");

			}catch(PDOException $e){
				echo $e->getMessage();
				exit();
			}
		}
	}

?>